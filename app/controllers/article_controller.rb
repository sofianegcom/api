class ArticleController < ApplicationController
  def index
    @articles = Article.all
  end

  def create
    @article = Article.new(article_params)
    @article.save
  end

  def show
    @article = Article.find(params[:id])
  end

  def update
    @article = Article.find(params[:id])
    @article.update(article_params)
    @article.save
  end

  def destroy
    @article = Article.find(params[:id])
    @article.destroy
  end
  private
    def article_params
      params.require(:article).permit(:title, :content, :category)
    end
end
