class Article
  include Mongoid::Document
  field :title, type: String
  field :content, type: Text
  field :write_date, type: DateTime
  field :last_edit_date, type: DateTime
  field :publish_date, type: DateTime
  field :category, type: String
end
