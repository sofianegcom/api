Rails.application.routes.draw do
  namespace :defaults => {:format => :json} do
    # Articles collection
    get       '/articles',      to: 'article#index'
    post      '/articles',      to: 'article#create'
    get       '/articles/:id',  to: 'article#show'
    put       '/articles/:id',  to: 'article#update'
    delete    '/articles/:id',  to: 'article#destroy'
  end
end
